import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Location extends StatefulWidget {
  const Location({super.key});

  @override
  State<Location> createState() => _LocationState();
}

class _LocationState extends State<Location> {
  List cities = [
    {'name':'Jakarta', 'timezone': 'Asia/Jakarta'},
    {'name':'Kuala Lumpur', 'timezone': 'Asia/Kuala_Lumpur'},
    {'name':'London', 'timezone': 'Europe/London'},
  ];

  List<Widget> content() {
    return cities.map((city) =>
      Card(
        margin: EdgeInsets.fromLTRB(4, 4, 4, 4),
        color: Colors.grey[400],
        child: ListTile(
          onTap: () async {
            var url = Uri.https('worldtimeapi.org', "/api/timezone/${city['timezone']}");
            var response = await http.get(url);
            var timezone = jsonDecode(response.body);
            print(timezone['datetime']);
            String time = timezone['datetime'];
            time = time.substring(11, 16);
            Navigator.pushReplacementNamed(context, '/home', arguments: {
              'city': city['name'],
              'datetime':time
            });
          },
          title: Text(city['name']),
        ),
      )
    ).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pilih Bandar"),
      ),
      body: ListView(
        children: content()
      ),
    );
  }
}
