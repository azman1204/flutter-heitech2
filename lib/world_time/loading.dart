import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading extends StatefulWidget {
  const Loading({super.key});

  @override
  State<Loading> createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  String msg = 'Loading...';
  // function ini run first time object di create (Widget Lifecycle)
  // initState() -> build() -> destroy()
  @override
  void initState() {
    super.initState();
    print('init..');
    getData();
  }

  void getData() async {
    try {
      var url = Uri.http('worldtimeapi.org', '/api/timezone/Asia/Kuala_Lumpur');
      var response = await http.get(url);
      var timezone = jsonDecode(response.body);
      print(timezone['datetime']);
      String time = timezone['datetime'];
      time = time.substring(11, 16);
      Navigator.pushNamed(context, '/home', arguments: {
        'city': 'Kuala Lumpur',
        'datetime':time
      });
    } catch (e) {
      // connection failed
      setState(() {
        msg = 'Connection failed!';
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    print('build...$msg');
    const spinkit = SpinKitFoldingCube(
      color: Colors.amber,
      size: 50.0,
    );

    return const MaterialApp(
      home: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.blueAccent,
          body: spinkit
          ),
        ),
    );
  }
}
