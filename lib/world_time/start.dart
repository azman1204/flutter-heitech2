import 'package:flutter/material.dart';

import 'home.dart';
import 'loading.dart';
import 'location.dart';

class Start extends StatelessWidget {
  const Start({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => Loading(),
        '/home': (context) => Home(),
        '/location': (context) => Location()
      },
    );
  }
}
