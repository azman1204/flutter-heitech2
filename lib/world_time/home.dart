import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    var timezone = ModalRoute.of(context)
        !.settings.arguments as Map;
    return Scaffold(
      appBar: AppBar(
        title: Text('City & Time'),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          children: [
            SizedBox(height: 60,),
            Text(
              timezone['city'],
              style: TextStyle(
                fontSize: 30
              ),
            ),
            Text(
              timezone['datetime'],
              style: TextStyle(
                fontSize: 80,
                color: Colors.red,
                fontWeight: FontWeight.bold
              ),
            ),
            TextButton.icon(
              onPressed: () {
                Navigator.pushNamed(context, '/location');
              },
              icon: Icon(Icons.map),
              label: Text('Pilih Bandar'),
              style: TextButton.styleFrom(
                backgroundColor: Colors.red,
              ),
            )
          ],
        ),
      ),
    );
  }
}
