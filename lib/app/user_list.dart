import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'config.dart';

class UserList extends StatefulWidget {
  const UserList({super.key});

  @override
  State<UserList> createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  List users = [];

  @override
  void initState() {
    super.initState();
    getUsers();
  }

  void getUsers() async {
    var url = Uri.http(ipaddr, '/flutter_api/user_list.php');
    var response = await http.get(url);
    var users = jsonDecode(response.body);
    print(users);
    setState(() {
      this.users = users;
    });
  }

  List<Widget> getCard() {
    List<Widget> lst =  [];
    for(var user in users) {
      lst.add(
          Card(
            color: Colors.grey[200],
            child: ListTile(
              title: Text(user['name']),
              subtitle: Text(user['email']),
              trailing: TextButton.icon(
                onPressed: (){
                  print(user['name']);
                  setState(() {
                    users.removeWhere((element)=> element['email'] == user['email']);
                  });

                  var url = Uri.http(ipaddr, '/flutter_api/delete.php', {'email':user['email']});
                  var response = http.get(url);
                },
                icon: Icon(Icons.delete_outline),
                label: Text('')),
            ),
          )
      );
    }
    return lst;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        // appBar: AppBar(
        //   title: Text('Senarai Pengguna Sistem'),
        //   centerTitle: true,
        //   elevation: 0,
        // ),
        body: ListView(
          children: getCard(),
        ),
      ),
    );
  }
}
