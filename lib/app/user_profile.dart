import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class UserProfile extends StatefulWidget {
  const UserProfile({super.key});

  @override
  State<UserProfile> createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  Map profile = {'name': '', 'age': ''};

  @override
  void initState() {
    super.initState();
    getProfile();
  }

  void getProfile() async {
    var url = Uri.http('192.168.137.1', '/flutter_api/users.php');
    var response = await http.get(url);
    var users = jsonDecode(response.body);
    print(users);
    setState(() {
      profile = users;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        //appBar: AppBar(title: const Text('User Profile')),
        body: Padding(
          padding: const EdgeInsets.fromLTRB(80.0, 40, 0, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 20,),
              Text('NAME : ${profile['name']}', style: TextStyle(fontSize: 30),),
              Text('AGE : ${profile['age']}', style: TextStyle(fontSize: 20),)
            ],
          ),
        ),
      ),
    );
  }
}
