import 'package:flutter/material.dart';

class WiridCalculator extends StatefulWidget {
  const WiridCalculator({super.key});

  @override
  State<WiridCalculator> createState() => _WiridCalculatorState();
}

class _WiridCalculatorState extends State<WiridCalculator> {
  int bil = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Wirid Calculator'),
        ),
        body: Center(
          child: Text(
            'Bilangan Wirid : $bil',
            style: TextStyle(
              fontSize: 30,
              color: Colors.green,
              fontWeight: FontWeight.bold
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              bil++;
            });
          },
          child: Icon(
            Icons.add
          ),
        ),
      ),
    );
  }
}
