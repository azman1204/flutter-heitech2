import 'package:flutter/material.dart';
import 'package:training/app/registration.dart';
import 'package:training/app/user_list.dart';
import 'package:training/app/user_profile.dart';

class TabDemo extends StatelessWidget {
  const TabDemo({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            bottom: const TabBar(
              tabs: [
                Tab(icon: Icon(Icons.directions_boat),),
                Tab(icon: Icon(Icons.directions_car),),
                Tab(icon: Icon(Icons.directions_bike),),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              UserProfile(),
              UserList(),
              Registration(),
            ],
          ),
        ),
      ),
    );
  }
}
