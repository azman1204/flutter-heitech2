import 'config.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Registration extends StatelessWidget {
  Registration({super.key});

  String name = '';
  String email = '';
  String password = '';

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Name',),
          TextField(
            decoration: InputDecoration(hintText: 'Your name'),
            onChanged: (val) {
              name = val;
            },
          ),
          SizedBox(height: 15,),
          Text('Email'),
          TextField(
            onChanged: (val) {
              email = val;
            },
          ),
          SizedBox(height: 15,),
          Text('Password'),
          TextField(
            onChanged: (val) {
              password = val;
            },
          ),
          SizedBox(height: 15,),
          ElevatedButton(onPressed: (){
            print('$name $email $password');
            var url = Uri.http(ipaddr, '/flutter_api/register.php');
            http.post(url, body: {
              'name': name,
              'email': email,
              'password': password
            });
          }, child: Text('Save'))
        ],
      ),
    );
  }
}
