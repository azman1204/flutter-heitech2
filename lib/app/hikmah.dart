import 'package:flutter/material.dart';

class Hikmah extends StatelessWidget {
  Hikmah({super.key});

  List<String> kata2 = [
    'Usaha tangga kejayaan',
    'Hati yang utuh adalah hati yang pernah patah',
    'Tenang, sabar dan diam mampu menyelesaikan banyak perkara'
  ];

  List<Widget> content() {
    List<Widget> result =  kata2.map((kata) {
      return Container(
         color: Colors.grey[200],
         child: Text('$kata'),
         padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
         margin: EdgeInsets.fromLTRB(0, 3, 0, 3),
      );
    }).toList();
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Kata Kata Hikmah'),
        ),
        body: ListView(
          children: content(),
        ),
      ),
    );
  }
}
