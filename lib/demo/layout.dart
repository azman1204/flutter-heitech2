///  layout in Flutter using Row and Column widgets
import 'package:flutter/material.dart';

class MyLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: Text('Layout Demo'),
            centerTitle: true,
            backgroundColor: Colors.pinkAccent,
          ),
          body: Row(
            children: [
              Container(
                color: Colors.blue[100],
                child: Text('Container 1'),
                padding: EdgeInsets.all(20.0),
              ),
              Container(
                color: Colors.amber,
                child: Text('Container 2'),
                padding: EdgeInsets.all(20.0),
              ),
              Column(
                children: [
                  Container(
                    color: Colors.red,
                    child: Text('Container 3'),
                    padding: EdgeInsets.all(20.0),
                  ),
                  Container(
                    color: Colors.green,
                    child: Text('Container 4'),
                    padding: EdgeInsets.all(20.0),
                  ),
                ],
              )
            ],
          ),
        )
    );
  }
}