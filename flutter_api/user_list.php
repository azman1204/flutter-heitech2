<?php
include 'conn.php';
$sql = "SELECT * FROM users";
$rs = mysqli_query($conn, $sql);
$arr = [];
while($row = mysqli_fetch_array($rs)) {
    $user = [
        'name'  => $row['name'],
        'email' => $row['email']
    ];
    $arr[] = $user;
}
//var_dump($arr);
header('Content-Type:application/json; charset=utf-8');
echo json_encode($arr);